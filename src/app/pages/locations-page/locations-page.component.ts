import { Component, OnInit } from '@angular/core';
// import { CharactersService } from 'src/app/shared/services/characters.service';
import { LocationsService } from 'src/app/shared/services/locations.service';

@Component({
  selector: 'app-locations-page',
  templateUrl: './locations-page.component.html',
  styleUrls: ['./locations-page.component.scss']
})
export class LocationsPageComponent implements OnInit {

  locations;
  paginationInfo  = {};

  constructor(private locationsService: LocationsService) { }

  ngOnInit(): void {
    this.getLocations(1);
  }

  getLocations(page) {
    this.locationsService.getLocations(page).subscribe((res: any) => {
      this.locations = res.results;
      this.paginationInfo = {​​ ...res.info, page };​​
      console.log(this.paginationInfo);
    })
  }

}
