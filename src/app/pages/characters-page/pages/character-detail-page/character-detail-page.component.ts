import { CharactersService } from 'src/app/shared/services/characters.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-character-detail-page',
  templateUrl: './character-detail-page.component.html',
  styleUrls: ['./character-detail-page.component.scss']
})
export class CharacterDetailPageComponent implements OnInit {

  //idCharacter: any;
  characterDetail;

  constructor(private route: ActivatedRoute, private CharactersService: CharactersService) {​​ }

  ngOnInit() {​​
    this.route.paramMap.subscribe(params => {​​
    //this.idCharacter = params.get('idCharacter');
    const idCharacter = params.get('idCharacter');
    this.showCharacterDetail(idCharacter);
    }​​);    
  }
  
  showCharacterDetail(id) {
    this.CharactersService.getCharacterDetail(id).subscribe((res: any) => {
      this.characterDetail = ​​res;
       console.log(res);
    })
  }
}
