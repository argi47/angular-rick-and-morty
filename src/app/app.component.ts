import { HomePageComponent } from './../../../angular-routing/src/app/pages/home-page/home-page.component';
import { Routes } from '@angular/router';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-rick-and-morty';
}
