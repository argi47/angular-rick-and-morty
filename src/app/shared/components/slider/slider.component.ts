import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  @Input() paginationInfo: any = {};
  @Output() newPageEmitter = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  newPageEmit(page){
    this.newPageEmitter.emit(page)
  }

  // previousContent() {
  //   // if (this.paginationInfo.prev != null) {
  //   //   console.log('previous');
  //   // }
  //   console.log('previous');    
  // }

  // nextContent() {
  //   // if (this.paginationInfo.next != null) {
  //   //   console.log('next');
  //   // }    
  //   console.log('next');
  // }

}
