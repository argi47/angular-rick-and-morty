import { Component, Input, OnInit } from '@angular/core';
import { FavoritesLocalService } from '../../services/local/favorites-local.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  @Input() list;
  @Input() flagShowFavoriteButton: boolean = true; //Lo usamos para esconder el botón "Add to favorites" si estamos en la pestaña Favorites
  
  constructor(private favoritesLocalService: FavoritesLocalService) { }

  ngOnInit(): void {
  }

  addOrRemoveToFavorites($event, newFavorite) {
    $event.stopPropagation(); //Con esto evitamos que haga el routerLink del figure

    if (this.flagShowFavoriteButton) {
      this.favoritesLocalService.addFavorite(newFavorite);
      console.log(this.favoritesLocalService.getFavorites());
    } else {
      this.favoritesLocalService.removeFavourite(newFavorite);
    }   

  }

}
