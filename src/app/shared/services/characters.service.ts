import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  constructor(private http: HttpClient) { }

  /*getCharacters() {
    return this.http.get('https://rickandmortyapi.com/api/character');
  }*/

  getCharacters(page) {
    return this.http.get('https://rickandmortyapi.com/api/character?page=' + page);    
  }

  getCharacterDetail(id) {
    return this.http.get('https://rickandmortyapi.com/api/character/' + id);    
  }
}
